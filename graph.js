/*!
 * MDE Graph 1.3
 * http://bitbucket.org/mdvanes/mdegraph/
 * http://mdworld.nl
 */
/*
==Notes==
Raw coordinates are relative to the upper left corner of the canvas element
Normalized coordinates are relative origin of the graph (y axis going upwards, x axis going right)

Please reserve the width of your graph + this.maxLabelWidth as the total width used, so labels can extend past the right end
of the graph.


==Documentation==

In html several things are required. A HTML snippet:
<div id="graphname"></div>

A javascript call (see at the bottom of example.html).

And some CSS classes for styling, all attributes are required:

#graphname_innerWrapper
{
	height: 330px;
	position: relative;
}
#graphname_static
{
	height: 330px;
	left: 0;
	position: absolute;
	top: 0;
	width: 440px;
}

And if the graph has labels, also this class:
#graphname_dynamic
{
	left: 0;
	position: absolute;
	top: 0;
}


==Parameters for the Graph constructor==
name - the name the graph will have (used to generate the canvas and inner wrapper names)
wrapperId - the id of the div that will contain the graph
xUnit:18,
yUnit:26,
xOffset:1991,
yOffset:0,
enableXTicks:true,
enableBullets:true,
enableBulletLabels - labels as defined in the coord list, showed when hovering over the bullet on that coordinate.
*/
var graph =
{
	Graph : function( params )
	{
		this.name = params.name;
		this.wrapperId = params.wrapperId;

		this.canvas;
		this.context;
		this.dynamicCanvas; // for bullet labels onmousemove
		this.dynamicContext;		
		this.innerWrapper; // for dynamicCanvas offset
		this.debug = document.getElementById( this.name + "_debug" );
		
		this.xMargin = 50;
		this.yMargin = 40;
		
		// unit size
		this.xUnit = params.xUnit; /* 1 value of x corresponds to xUnit pixels on the x axis */
		this.yUnit = params.yUnit; /* 1 value of y corresponds to yUnit pixels on the y axis */
		
		// axis offset (start labels at these values)
		this.xOffset = params.xOffset; /* xOffset is displayed at 0 on the x axis */
		this.yOffset = params.yOffset;
		
		this.xLabelY; /* raw y coordinate of the labels on the x axis */
		this.yLabelX = 5; /* raw x coordinate of the labels on the y axis */
		
		this.xLabels = params.xLabels;
		this.yLabels = params.yLabels;
		this.lines = params.lines;
		this.rawLines;
		
		this.colors = [ "#3366cc", "#dc3912" ];
		this.defaultColor = "#0f0";
		
		this.enableXTicks = params.enableXTicks;
		
		// draw filled circles on the coords
		this.enableBullets = params.enableBullets;
		this.bulletRadius = 4;
		
		// mouse position
		this.x;
		this.y;

		// bullet label
		this.enableBulletLabels = params.enableBulletLabels;
		this.minLabelWidth = 30; // minimal label width
		this.maxLabelWidth = 200; // maximal label width, truncate beyond this width
		
		this.init = function()
		{
			if( this.debug != null )
			{
				this.debug.innerHTML = "<h2>Debug is enabled</h2>";
			}

			this.createStaticCanvas();
			
			// xLabelY can only be calculated after this.canvas.height is set.
			this.xLabelY = this.canvas.height - 12;
		
			this.context = this.canvas.getContext("2d");
			
			if( this.enableBulletLabels )
			{
				this.createDynamicCanvas();
				if( this.dynamicCanvas != null )
				{
					this.dynamicContext = this.dynamicCanvas.getContext("2d");
				}
			}

			this.drawXLabels( this.xLabels );
			this.drawYLabelsAndGuidelines( this.yLabels );
			
			this.rawLines = new Array(this.lines.length);
			for( var i = 0; i < this.lines.length; i++ )
			{
				this.rawLines[i] = this.drawLine( this.lines[i], this.getColor(i) );
			}
		}
		
		this.redraw = function()
		{
			this.reset();
			// recalculate bulletlabels
			this.drawXLabels( this.xLabels );
			this.drawYLabelsAndGuidelines( this.yLabels );
			this.rawLines = new Array(this.lines.length);
			for( var i = 0; i < this.lines.length; i++ )
			{
							this.rawLines[i] = this.drawLine( this.lines[i], this.getColor(i) );
			}

			// Rebind the trackBulletLabel, with the updated coordinates
			this.dynamicCanvas.onmousemove = null; //removeEventListener( 'mousemove' );

			var _this = this;
			this.dynamicCanvas.addEventListener( 'mousemove', 
			 function(event){ 
				_this.trackBulletLabel(event);
			 }, false );
		}

		/*
		@function
		Create first canvas for the graph itself
		*/
		this.createStaticCanvas = function()
		{
			var innerWrapperId = this.name + "_innerWrapper";
			var innerWrapper = document.createElement("div");
			innerWrapper.setAttribute( "id", innerWrapperId );
			document.getElementById( this.wrapperId ).appendChild( innerWrapper );
			this.innerWrapper = document.getElementById( innerWrapperId );
			
			var staticCanvasId = this.name + "_static";
			var staticCanvas = document.createElement("canvas");
			staticCanvas.setAttribute( "id", staticCanvasId );
			this.innerWrapper.appendChild( staticCanvas );
			this.canvas = document.getElementById( staticCanvasId );
			
			// Get width and height from CSS, but width and height attributes are needed: http://stackoverflow.com/questions/2588181/canvas-is-stretch-when-using-css-but-normal-with-old-width-and-height-propert
			// TODO use jquery to normalize across browsers
			this.canvas.setAttribute('width', this.canvas.clientWidth);
			this.canvas.setAttribute('height', this.canvas.clientHeight);
		}

		/*
		@function
		Create second canvas for dynamic elements, e.g. labels
		*/
		this.createDynamicCanvas = function()
		{
			var dynamicCanvasId = this.name + "_dynamic";
			var dynamicCanvas = document.createElement("canvas");
			dynamicCanvas.setAttribute( "id", dynamicCanvasId );
			// Allow for labels to extend past the right end of the graph
			dynamicCanvas.setAttribute( "width", this.canvas.width + this.maxLabelWidth );
			dynamicCanvas.setAttribute( "height", this.canvas.height );
			this.innerWrapper.appendChild( dynamicCanvas );
			this.dynamicCanvas = document.getElementById(dynamicCanvasId);
			
			var _this = this;
			this.dynamicCanvas.addEventListener( 'mousemove', 
			 function(event){ 
				_this.trackBulletLabel(event);
			 }, false );

		}
		
		/*
		@function
		Draw label when hovering over a bullet.
		Called each time the mouse cursor moves over the canvas.
		The second canvas, for dynamic content like these labels, is transparent and positioned on top of the first canvas,
		for static content, to prevent having to redraw the graph each mousemove.
		*/
		this.trackBulletLabel = function(event)
		{
			if( this.dynamicCanvas == null )
			{
				alert( "No canvas is available to draw bullet labels." );
				return;
			}
			this.getMousePosition(event);
      
			// Check if the current position is within the radius of any bullet of any line
			// For any line:
			lines:
			for( var i = 0; i < this.rawLines.length; i++ )
			{
				var line = this.rawLines[i];
				// For any bullet:
				for( var j = 0; j < line.length; j++ )
				{
					var coord = line[j];
					if( ( this.x > coord.x - this.bulletRadius) && 
							( this.x < coord.x + this.bulletRadius) && 
							( this.y > coord.y - this.bulletRadius) && 
							( this.y < coord.y + this.bulletRadius) )
			    {
			    	// TODO remove logging
			    	if( this.debug != null )
			    	{
				      this.debug.innerHTML = "x: " + this.x + "  y: " + this.y + "<br/>canvas offsetLeft="+this.canvas.offsetLeft 
					+"<br/>innerWrapper offsetLeft=" + this.innerWrapper.offsetLeft +"<br/> label="+coord.label ;
			    	}
			
						// Reset dynamicCanvas
						this.dynamicCanvas.width = this.dynamicCanvas.width;
						
						// float behind cursor: 
						//this.drawBulletLabel( this.x, this.y, coord.label );
						// float behind bullet:
						this.drawBulletLabel( coord.x, coord.y, coord.label );
						break lines;
			    }
					else
					{
						// Reset dynamicCanvas here to remove the label onmouseout of the bullet
						this.dynamicCanvas.width = this.dynamicCanvas.width;
					}
				}
			}
		}
		
		this.drawBulletLabel = function( x, y, label )
		{
			this.dynamicContext.fillStyle = "#fff";
			this.dynamicContext.strokeStyle = "#000";
			this.dynamicContext.lineWidth = 1;
			
			this.dynamicContext.beginPath();
			
			var rY = Math.round(y);
			var rX = Math.round(x);
			
			var labelWidth = this.minLabelWidth;
			var measuredTextWidth = this.dynamicContext.measureText(label).width;
			
			if( label == undefined )
			{
				return;
			}
			else if( measuredTextWidth > this.maxLabelWidth )
			{
				labelWidth = this.maxLabelWidth;
				label = this.getFittingLabel( this.maxLabelWidth, label );
			}
			else if( measuredTextWidth > this.minLabelWidth )
			{
				labelWidth = measuredTextWidth;
			}

			// paint box
			this.dynamicContext.moveTo( rX - 10.5, rY + 24.5 );
			this.dynamicContext.lineTo( rX - 5.5, rY + 24.5 );
			this.dynamicContext.lineTo( rX + 0.5, rY + 19.5 );
			this.dynamicContext.lineTo( rX + 5.5, rY + 24.5 );
			this.dynamicContext.lineTo( rX + labelWidth + 0.5, rY + 24.5 );
			this.dynamicContext.lineTo( rX + labelWidth + 0.5, rY + 24.5 );
			this.dynamicContext.lineTo( rX + labelWidth + 0.5, rY + 42.5 );
			this.dynamicContext.lineTo( rX - 10.5, rY + 42.5 );
			this.dynamicContext.lineTo( rX - 10.5, rY + 24.5 );
			this.dynamicContext.fill();
			this.dynamicContext.stroke();
			this.dynamicContext.closePath();
			// paint text
			this.dynamicContext.fillStyle = "#000";
			this.dynamicContext.fillText( label, x - 5, y + 37 );
		}
		
		// Iterate over the string substracting the last character each iteration until it fits in the label
		this.getFittingLabel = function( maxWidth, label )
		{
			var measuredTextWidth = this.dynamicContext.measureText(label).width;
			if( measuredTextWidth > maxWidth )
			{
				label = label.substr(0,label.length - 1);
				return this.getFittingLabel( maxWidth, label );
			}
			else
			{
				return label;
			}
		}
		
		this.getMousePosition = function(event)
		{
			// Thanks to Ryan Artecona on http://stackoverflow.com/a/5932203
			var totalOffsetX = 0;
			var totalOffsetY = 0;
			var currentElement = this.canvas;

			do{
				totalOffsetX += currentElement.offsetLeft;
				totalOffsetY += currentElement.offsetTop;
			}
			while(currentElement = currentElement.offsetParent)

			this.x = event.pageX - totalOffsetX;
			this.y = event.pageY - totalOffsetY;
		}
		
		/*
		@function
		Pre-defined colors.
		If more colors than available in the predefined color list, return the default color.
		*/
		this.getColor = function( lineIndex )
		{
			if( lineIndex > (this.colors.length - 1) )
			{
				return this.defaultColor;
			}
			else
			{
				return this.colors[lineIndex];
			}
		}
		
		this.drawXLabels = function( labels )
		{
			for( var i = 0; i < labels.length; i++ )
			{
				this.drawXLabel( labels[i].x, labels[i].text );
			}
		}
		
		// Supply scaled x coordinate and text
		this.drawXLabel = function( x, text )
		{
			var scaledX = (this.xMargin ) + ((x - this.xOffset) * this.xUnit);
			this.drawRawXLabel( scaledX, text );
		}

		// Supply raw x coordinate and text
		this.drawRawXLabel = function( x, text )
		{
			if( this.enableXTicks )
			{
				// draw ticks (2 x 1px markers) on x axis
			  this.context.moveTo( x + 0.5, this.xLabelY - 27 );
			  this.context.lineTo( x + 0.5, this.xLabelY - 25 );
			  this.context.stroke();
			}

			// draw label
			this.context.save();
			this.context.beginPath();
			this.context.translate( x, this.xLabelY );
			this.context.rotate(-0.5);
			this.context.fillText( text, 0, 0);
			this.context.restore();
		}
		
		/*
		@function
		Convert a list of pairs of y coordinates and texts to drawn y labels and guidelines.
		*/
		this.drawYLabelsAndGuidelines = function(labels)
		{
			this.drawYLabelAndGuideline( labels[0].y, labels[0].text, true );
			for( var i = 1; i < labels.length; i++ )
			{
				this.drawYLabelAndGuideline( labels[i].y, labels[i].text, false );
			}
		}

		/*
		@function
		Draws label on y axis and guideline over the entire width of the graph at this y coordinate
		@param y Y coordinate in the same unit as the graph line points.
		@param text The text of the label
		@param first Boolean if this is the first guideline (relative to the origin of the graph). 
		The first line is drawn in black, the rest in grey.
		*/
		this.drawYLabelAndGuideline = function( y, text, first )
		{
			var scaledY = this.canvas.height - this.yMargin - ((y - this.yOffset) * this.yUnit);
			// Adding 0.5 so a 1px wide line is not drawn at half opacity across two pixels.
			// See "A lineWidth example" https://developer.mozilla.org/En/Canvas_tutorial/Applying_styles_and_colors
			scaledY = scaledY + 0.5;
			this.drawRawYLabelAndGuideline( scaledY, text, first );
		}

		/*
		@function
		Draws label on y axis and guideline over the entire width of the graph at this y coordinate
		@param y Raw y coordinate
		@param text The text of the label
		@param first Boolean if this is the first guideline (relative to the origin of the graph). 
		The first line is drawn in black, the rest in grey.
		*/
		this.drawRawYLabelAndGuideline = function( y, text, first )
		{
			this.context.save();
			this.context.beginPath();
			this.context.fillText( text, this.yLabelX, y + 3 ); // correct label y coordinate to center of guideline
			this.context.restore();

			this.context.save();
			if( first )
			{
				this.context.strokeStyle = "#000";
			}
			else
			{
				this.context.strokeStyle = "#eee";
			}
		  this.context.moveTo( this.xMargin, y );
		  this.context.lineTo( this.canvas.width - 5, y );
		  this.context.stroke();
			this.context.restore();
		}
		
		this.drawLine = function( coords, color )
		{
			var rawCoords = new Array(coords.length);
		  for( var i = 0; i < coords.length; i++ )
		  {
		  	var x = this.xMargin + ((coords[i].x - this.xOffset) * this.xUnit);
		  	var y = this.canvas.height - this.yMargin - ((coords[i].y - this.yOffset) * this.yUnit);
			rawCoords[i] = { "x":x, "y":y, "label":coords[i].label };		  	
		  }
		  this.drawLineRaw( rawCoords, color );
		  return rawCoords;
		}
		
		this.drawLineRaw = function( coords, color )
		{
			this.context.beginPath();
		  this.context.moveTo(coords[0].x, coords[0].y);
		  for( var i = 1; i < coords.length; i++ )
		  {
		  	this.context.lineTo(coords[i].x, coords[i].y);
		  	if( this.exeedsDimensions(coords[i]) )
		  	{
		  		return;
		  	}
		  }
			this.context.strokeStyle = color;
			this.context.lineWidth = 2;
			this.context.stroke();
			
			if( this.enableBullets )
			{
				// paths for bullets may not interfere with the path for the lines
				this.drawBulletsRaw( coords, color );
			}
		}
		
		this.drawBulletsRaw = function( coords, color )
		{
		  for( var i = 0; i < coords.length; i++ )
		  {
				this.context.beginPath();
				this.context.fillStyle = color;
				this.context.arc( coords[i].x, coords[i].y, this.bulletRadius, 0, Math.PI*2, true );
				this.context.closePath();
				this.context.fill();
		  }
		}
		
		this.exeedsDimensions = function(coords)
		{
			if(coords.x > this.canvas.width)
			{
				alert( "Exceeds dimensions\ncanvas width=" + this.canvas.width + " x=" + coords.x  );
				return true;
			}
			else if(coords.y > this.canvas.height)
			{
				alert( "Exceeds dimensions\ncanvas height=" + this.canvas.height + " y=" + coords.y  );
				return true;
			}
			else
			{
				return false;
			}
		}

		this.reset = function()
		{
			this.canvas.width = this.canvas.width;
		}
	}
}
